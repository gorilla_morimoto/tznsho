// 正規表現の練習

package regex_pract

import (
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
)

func main() {
	os.Create("hoge.xls")
	os.Create("fuga.xlsx")

	cdir, _ := os.Getwd()
	dirItems, _ := ioutil.ReadDir(cdir)
	r := regexp.MustCompile(`.xls$|.xlsx$`)
	for _, v := range dirItems {
		if r.MatchString(v.Name()) {
			fmt.Println(v.Name())
		}
	}
}
